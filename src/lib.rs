// https://docs.rs/slog/2.4.1/slog/

#[macro_use]
extern crate slog;
#[macro_use]
extern crate chan;
extern crate futures;
extern crate hyper;
extern crate mime;
#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate prometheus;
extern crate tokio;

#[macro_use]
extern crate serde_derive;

extern crate serde;
#[macro_use]
extern crate serde_json;

use futures::future::Loop;
use futures::future::LoopFn;
use futures::future::{err, ok, Future};
use hyper::header::{HeaderValue, CONTENT_TYPE};
use hyper::service::service_fn_ok;
use hyper::{Body, Method, Response, Server, StatusCode};
use tokio::prelude::FutureExt;

use mime::Mime;
use slog::Level;
use slog::Logger;
use std::time::Duration;

use prometheus::{Counter, Encoder, TextEncoder};

#[derive(Serialize, Deserialize, Debug)]
struct Alert {
    status: String,
    labels: serde_json::Map<String, serde_json::Value>,
    annotations: serde_json::Map<String, serde_json::Value>,
}

pub struct Config {
    pub am_url: String,
    pub interval: Duration,
    pub log_level: Level,
    pub service_name: String,
}

/*
    ticksTotal = prometheus.NewCounter(
        prometheus.CounterOpts{
            Name: "deadman_ticks_total",
            Help: "The total ticks passed in this snitch",
        },
    )

    ticksNotified = prometheus.NewCounter(
        prometheus.CounterOpts{
            Name: "deadman_ticks_notified",
            Help: "The number of ticks where notifications were sent.",
        },
    )

    failedNotifications = prometheus.NewCounter(
        prometheus.CounterOpts{
            Name: "deadman_notifications_failed",
            Help: "The number of failed notifications.",
        },
    )
)

func init() {
    prometheus.MustRegister(
        ticksTotal,
        ticksNotified,
        failedNotifications,
    )
}
*/
lazy_static! {
    static ref PROM_TICKS_TOTAL: Counter = register_counter!(opts!(
        "deadman_ticks_total",
        "The total ticks passed in this snitch",
        labels! {"handler" => "all",}
    ))
    .unwrap();
    static ref PROM_TICKS_NOTIFIED: Counter = register_counter!(opts!(
        "deadman_ticks_notified",
        "The number of ticks where notifications were sent.",
        labels! {"handler" => "all",}
    ))
    .unwrap();
    static ref PROM_FAILED_NOTIFICATIONS: Counter = register_counter!(opts!(
        "deadman_notifications_failed",
        "The number of failed notifications.",
        labels! {"handler" => "all",}
    ))
    .unwrap();
}

/*
type Deadman struct {
    pinger   <-chan time.Time
    interval time.Duration
    ticker   *time.Ticker
    closer   chan struct{}

    notifier func() error

    logger log.Logger
}

*/

/*ticker: Option<chan::Receiver<chan::Sender<()>>>,*/
/*notifier: Box<Fn() -> Result<(), String>>, */
pub struct Deadman {
    pinger_send: chan::Sender<()>,
    pinger_recv: chan::Receiver<()>,
    interval: Duration,
    notifier: String,
    closer: (chan::Sender<()>, chan::Receiver<()>),
    server_closer: Option<futures::sync::oneshot::Sender<()>>,
    logger: Logger,
    service_name: String,
}

impl Deadman {
    /*
    func NewDeadMan(pinger <-chan time.Time, interval time.Duration, amURL string, logger log.Logger) (*Deadman, error) {
            return newDeadMan(pinger, interval, amNotifier(amURL), logger), nil
    }
    func newDeadMan(pinger <-chan time.Time, interval time.Duration, notifier func() error, logger log.Logger) *Deadman {
            return &Deadman{
                    pinger:   pinger,
                    interval: interval,
                    notifier: notifier,
                    closer:   make(chan struct{}),
                    logger:   logger,
            }
    }
    */
    pub fn new(
        interval: Duration,
        /* notifier: Box<Fn() -> Result<(), String>>, */
        notifier: String,
        logger: Logger,
        service_name: String,
    ) -> Deadman {
        let (pinger_send, pinger_recv) = chan::sync(0);
        Deadman {
            pinger_send: pinger_send,
            pinger_recv: pinger_recv,
            interval: interval,
            /*ticker: None,*/
            notifier: notifier,
            server_closer: None,
            closer: chan::sync(0),
            logger: logger,
            service_name: service_name,
        }
    }

    pub fn debug(&mut self, line: String) -> Result<(), ()> {
        debug!(self.logger, "{}", line);
        Ok(())
    }

    pub fn serve_status(&mut self) -> impl futures::future::Future<Item = (), Error = ()> {
        let addr = ([0, 0, 0, 0], 8080).into();

        let logger = self.logger.clone();
        let err_logger = self.logger.clone();

        let server = Server::bind(&addr)
            .serve(move || {
                let logger = logger.clone();
                service_fn_ok(move |req| {
                    debug!(logger, "==>");

                    let encoder = TextEncoder::new();
                    match (req.method(), req.uri().path()) {
                        (&Method::GET, "/metrics") => {
                            debug!(logger, "Received metrics request [status]");
                            let metric_families = prometheus::gather();
                            let mut buffer = vec![];
                            encoder.encode(&metric_families, &mut buffer).unwrap();
                            let mime = encoder.format_type().parse::<Mime>().unwrap();
                            Response::builder()
                                .header(CONTENT_TYPE, HeaderValue::from_str(mime.as_ref()).unwrap())
                                .body(Body::from(buffer))
                                .unwrap()
                        }
                        (&Method::GET, "/healthz") => {
                            debug!(logger, "Received liveness check [status]");
                            Response::builder().body(Body::from("ok")).unwrap()
                        }
                        (&Method::GET, "/") => {
                            debug!(logger, "Received / request [status]");
                            Response::builder().body(Body::from("")).unwrap()
                        }
                        _ => Response::builder()
                            .status(StatusCode::NOT_FOUND)
                            .body(Body::from(""))
                            .unwrap(),
                    }
                })
            })
            .map_err(move |err| {
                error!(err_logger, "Status Server: {}", err.to_string());
            });

        server
    }

    pub fn serve(&mut self) -> impl futures::future::Future<Item = (), Error = ()> {
        let addr = ([0, 0, 0, 0], 9095).into();

        let pinger_send = self.pinger_send.clone();
        let logger = self.logger.clone();
        let err_logger = self.logger.clone();

        let server = Server::bind(&addr)
            .serve(move || {
                let pinger_send = pinger_send.clone();
                let logger = logger.clone();
                service_fn_ok(move |req| {
                    debug!(logger, "-->");

                    match (req.method(), req.uri().path()) {
                        (&Method::GET, "/") => {
                            debug!(logger, "Received / request");
                            Response::builder().body(Body::from("")).unwrap()
                        }
                        (&Method::POST, "/alive") => {
                            debug!(logger, "Received ping");
                            pinger_send.send(());
                            Response::builder().body(Body::from("ok")).unwrap()
                        }
                        _ => Response::builder()
                            .status(StatusCode::NOT_FOUND)
                            .body(Body::from(""))
                            .unwrap(),
                    }
                })
            })
            .map_err(move |err| {
                error!(err_logger, "Server: {}", err.to_string());
            });

        server
    }

    /*
    func (d *Deadman) Run() error {
            d.ticker = time.NewTicker(d.interval)

            skip := false

            for {
                    select {
                    case <-d.ticker.C:
                            ticksTotal.Inc()

                            if !skip {
                                    ticksNotified.Inc()
                                    if err := d.notifier(); err != nil {
                                            failedNotifications.Inc()
                                            level.Error(d.logger).Log("err", err)
                                    }
                            }
                            skip = false

                    case <-d.pinger:
                            skip = true

                    case <-d.closer:
                            break
                    }
            }

            return nil
    }
    */
    pub fn start(&mut self) -> impl futures::future::Future<Item = (), Error = ()> {
        let ticker = chan::tick(self.interval);

        let mut skip = false;
        let (_, closer) = self.closer.clone();
        let pinger = self.pinger_recv.clone();
        let notifier = self.notifier.clone();
        let service_name = self.service_name.clone();
        let logger = self.logger.clone();
        let first = true;

        debug!(self.logger, "Starting loop...");
        debug!(self.logger, "Interval: {}s", self.interval.as_secs());

        futures::future::loop_fn(first, move |first| {
            if first {
                return ok::<futures::future::Loop<bool, bool>, ()>(
                    futures::future::Loop::Continue(false),
                );
            }
            let mut exit = false;
            chan_select! {
                ticker.recv() => {
                    PROM_TICKS_TOTAL.inc();

                    if ! skip {
                        PROM_TICKS_NOTIFIED.inc();
                        debug!(logger, "Ticks Notified: {}", PROM_TICKS_NOTIFIED.get());

                        let _notify = alertmanager_notifier(&notifier, &service_name).map_err(|err| {
                            PROM_FAILED_NOTIFICATIONS.inc();
                            /*error!(logger, "err {}", err);*/
                        });
                    }

                    skip = false;
                },
                pinger.recv() => {
                    info!(logger, "Ping received...");
                    skip = true;
                }
                /*
                closer.recv() => {
                    info!(logger, "Closing...");
                    exit = true;
                },
                */
            };
            if exit {
                ok::<futures::future::Loop<bool, bool>, ()>(futures::future::Loop::Break(false))
            } else {
                ok::<futures::future::Loop<bool, bool>, ()>(futures::future::Loop::Continue(false))
            }
        }).map(|_| ())
    }

    /*
    func (d *Deadman) Stop() {
            if d.ticker != nil {
                    d.ticker.Stop()
            }

            d.closer <- struct{}{}
    }
    */
    /*
    fn stop(&mut self) {
        match self.ticker {
            Some(ticker) => {
                // TODO: stop ticker first
            }
            None => {}
        }

        let (send, recv) = &self.closer;
        send.send(());
    }
        */
}

/*
func amNotifier(amURL string) func() error {
    alerts := []*model.Alert{{
        Labels: model.LabelSet{
            model.LabelName("alertname"): model.LabelValue("DeadmanDead"),
        },
    }}

    b, err := json.Marshal(alerts)
    if err != nil {
        fmt.Fprintln(os.Stderr, "error:", err)
        os.Exit(2)
    }

    return func() error {
        client := &http.Client{}
        resp, err := client.Post(amURL, "application/json", bytes.NewReader(b))
        if err != nil {
            return err
        }
        defer resp.Body.Close()

        if resp.StatusCode/100 != 2 {
            return fmt.Errorf("bad response status %v", resp.Status)
        }

        return nil
    }
}
            HTTP_COUNTER.inc();
*/
fn alertmanager_notifier(alertmanager_url: &String, service_name: &String) -> Result<(), ()> {
    let connector = hyper::client::HttpConnector::new(1);
    let client = hyper::client::Client::builder().build(connector);
    let uri: hyper::Uri = alertmanager_url.parse().unwrap();

    /*
    alerts := []*model.Alert{{
            Labels: model.LabelSet{
                    model.LabelName("alertname"): model.LabelValue("DeadmanDead"),
            },
    }}
    */

    let mut alert_labels = serde_json::Map::new();
    alert_labels.insert("alertname".to_string(), json!("DeadmanDead"));
    let mut alert_annotations = serde_json::Map::new();
    alert_annotations.insert(
        "summary".to_string(),
        json!(format!("Deadman:{}", service_name)),
    );
    alert_annotations.insert(
        "description".to_string(),
        json!(format!(
            "Deadman's switch triggered for service {}",
            service_name
        )),
    );

    let alerts = &vec![Alert {
        status: "firing".to_string(),
        labels: alert_labels,
        annotations: alert_annotations,
    }];

    let body = match serde_json::to_string(alerts) {
        Ok(value) => value,
        Err(err) => return Err(()),
    };
    let req = hyper::Request::builder()
        .method(Method::POST)
        .uri(uri)
        .header(CONTENT_TYPE, HeaderValue::from_static("application/json"))
        .body(Body::from(body))
        .unwrap();

    // FIXME: TIMEOUT as setting
    let fut = client
        .request(req)
        .timeout(Duration::from_secs(3))
        .then(|result| match result {
            Ok(res) => Ok(()),
            Err(err) => {
                println!("{}", "ALERTMANAGER ERROR");
                Err(())
            }
        });
    hyper::rt::spawn(fut);
    Ok(())
}

pub fn run(cfg: Config, logger: slog::Logger) -> Result<(), String> {
    /*
    pinger := make(chan time.Time)
    http.Handle("/metrics", promhttp.Handler())
    http.Handle("/", simpleHandler(pinger))
    go http.ListenAndServe(":9095", nil)
    */

    let am_url = cfg.am_url;
    // let notifier = Box::new(move || -> Result<(), String> { alertmanager_notifier(&am_url) });
    let notifier = am_url.clone();
    let mut deadman = Deadman::new(cfg.interval, notifier, logger, cfg.service_name);

    deadman.debug("[starting event loop]".to_string());
    hyper::rt::run(hyper::rt::lazy(move || {
        deadman.debug("[spawning counter]".to_string());
        hyper::rt::spawn(deadman.start());
        deadman.debug("[spawning serve]".to_string());
        hyper::rt::spawn(deadman.serve());
        deadman.debug("[spawning status serve]".to_string());
        hyper::rt::spawn(deadman.serve_status());
        deadman.debug("[spawns complete]".to_string());
        Ok(())
    }));

    Ok({})
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
