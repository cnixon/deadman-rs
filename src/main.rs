// https://github.com/clap-rs/clap

extern crate clap;
extern crate deadman;
extern crate humantime;

#[macro_use]
extern crate slog;
extern crate slog_async;
extern crate slog_term;

use slog::Drain;
use slog::LevelFilter;

use clap::{App, Arg};
use humantime::parse_duration;
use slog::Level;
use slog::Logger;

fn parse_configuration() -> Result<deadman::Config, String> {
    let matches = App::new("Deadman Rust")
        .version("0.1")
        .author("Phil Weir <phil.weir@flaxandteal.co.uk>")
        .about("A deadman's switch for Prometheus Alertmanager compatible notifications, based on gouthamve/deadman")
        .arg(
            Arg::with_name("alertmanager-url")
                .long("am.url")
                .value_name("URL")
                .help("The URL to POST alerts to.")
                .default_value("http://localhost:9093/api/v1/alerts")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("service-name")
                .long("deadman.service")
                .value_name("SERVICENAME")
                .help("A label for the service that Deadman is receiving from.")
                .default_value("UNDEFINED")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("interval")
                .long("deadman.interval")
                .value_name("INTERVAL")
                .help("The heartbeat interval. An alert is sent if no heartbeat is sent.")
                .default_value("30s")
                .takes_value(true),
        )
        // https://github.com/prometheus/common/blob/master/promlog/flag/flag.go
        .arg(
            Arg::with_name("log-level")
                .long("log.level")
                .value_name("LOGLEVEL")
                .help("Only log messages with the given severity or above. One of: [debug, info, warn, error]")
                .default_value("warn")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("log-format")
                .long("log.format")
                .value_name("LOGFORMAT")
                .help("[NOT-YET-IMPLEMENTED] Output of log messages. One of: [logfmt, json]")
                .default_value("logfmt")
                .takes_value(true),
        )
        .get_matches();

    /*
    app := kingpin.New(filepath.Base(os.Args[0]), "A deadman's snitch for Prometheus Alertmanager compatible notifications.")
    app.HelpFlag.Short('h')

    app.Flag("am.url", "The URL to POST alerts to.").
        Default("http://localhost:9093/api/v1/alerts").StringVar(&cfg.amURL)
    app.Flag("deadman.interval", "The heartbeat interval. An alert is sent if no heartbeat is sent.").
        Default("30s").SetValue(&cfg.interval)

    promlogflag.AddFlags(app, &cfg.logLevel)
    */

    // TODO: print help by default, if no args

    // Gets a value for config if supplied by user, or defaults to "default.conf"
    let am_url = matches.value_of("alertmanager-url").unwrap().to_string();
    let service_name = matches.value_of("service-name").unwrap().to_string();
    let interval = match parse_duration(matches.value_of("interval").unwrap()) {
        Ok(interval) => interval,
        Err(error) => {
            let error_msg = format!("Error parsing commandline arguments [duration: {}]", error);
            return Err(error_msg.to_string());
        }
    };
    let log_level = match matches.value_of("log-level") {
        Some("debug") => Level::Debug,
        Some("info") => Level::Info,
        Some("warn") => Level::Warning,
        Some("error") => Level::Error,
        _ => {
            let error_msg = format!("Error parsing commandline arguments [log-level]");
            return Err(error_msg.to_string());
        }
    };

    /*
    _, err := app.Parse(os.Args[1:])
    if err != nil {
        fmt.Fprintln(os.Stderr, errors.Wrapf(err, "Error parsing commandline arguments"))
        app.Usage(os.Args[1:])
        os.Exit(2)
    }
    */

    /*
    cfg := struct {
        amURL    string
        interval model.Duration
        logLevel promlog.AllowedLevel
    }{}
    */
    Ok(deadman::Config {
        am_url,
        interval,
        log_level,
        service_name,
    })
}

fn initialize_logger(log_level: Level) -> Result<Logger, String> {
    let decorator = slog_term::TermDecorator::new().build();
    let drain = slog_term::FullFormat::new(decorator).build().fuse();
    let drain = slog_async::Async::new(drain).build().fuse();
    let drain = LevelFilter::new(drain, log_level).fuse();

    Ok(Logger::root(drain, o!()))
}

fn main() {
    let cfg = parse_configuration()
        .map_err(|err| {
            std::process::exit(2);
        })
        .unwrap();

    let logger = initialize_logger(cfg.log_level)
        .map_err(|err| {
            std::process::exit(3);
        })
        .unwrap();

    /*
    logger := promlog.New(cfg.logLevel)
    */

    let _result = deadman::run(cfg, logger).map_err(|err| {
        std::process::exit(1);
    });

    std::process::exit(0);

    /*
    d, err := NewDeadMan(pinger, time.Duration(cfg.interval), cfg.amURL, log.With(logger, "component", "deadman"))
    if err != nil {
        level.Error(logger).Log("err", err)
        os.Exit(2)
    }

    d.Run()
    */
}
